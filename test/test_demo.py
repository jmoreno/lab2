import pytest

def test_suma ():
    print("Mi primer test con pytest (sólo se muestra la salida estándar si falla)")
    assert 3 + 2 == 5, f"{3 + 2} debería ser igual a {5}"

def test_con_float ():
    # Este test fallaría:
    # assert 100 / 3 == 33.33333

    # Este no falla:
    assert 100 / 3 == pytest.approx(33.33333)

@pytest.mark.parametrize(["m", "n", "salida_esperada"], [(1, 2, 3), (2, 5, 7)])
def test_suma_parametrizado (m, n, salida_esperada):
    assert m + n == salida_esperada


def test_debe_elevarse_excepcion():
    with pytest.raises(TypeError):
        assert 1 + "a", "Debería haberse elevado la excepción TypeError"
