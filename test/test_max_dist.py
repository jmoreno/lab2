from max_dist import max_dist
import pytest

def test_A1():
	with pytest.raises(Exception):
		assert max_dist(), "Debería lanzarse InvalidArgument"

def test_A2():
	with pytest.raises(Exception):
		assert max_dist([]), "Deberia lanzarse InvalidArgument"

def test_A3():
	with pytest.raises(Exception):
		assert max_dist([1]), "Deberia lanzarse InvalidArgument"

def test_TypeError():
	with pytest.raises(TypeError):
		assert max_dist(1,3,"a",9)
		
def test_B():
	assert max_dist([1,1]) == 0

def test_C1():
	assert max_dist([1,2]) == 1
		
def test_C2():
	assert max_dist([2,1]) == 1

def test_D():
	assert max_dist([1,4,9]) == 5

def test_E():
	assert max_dist([9,4,1]) == 5

def test_F():
	assert max_dist([4,9,1]) == 8

def test_G():
	assert max_dist([1,9,4]) == 8

def test_H():
	assert max_dist([4,1,9]) == 8

def test_I():
	assert max_dist([9,1,4]) == 8
