from dos_menores import dos_menores

def test_A1():
	assert dos_menores() == None

def test_A2():
	assert dos_menores([]) == None
	
def test_B():
	assert dos_menores([2]) == 2
	
def test_C1():
	assert dos_menores([3,2]) == (2,3)
	
def test_C2(): 
	assert dos_menores([2,3]) == (2,3)
	
def test_D():
	assert dos_menores ([4,6,3]) == (3,4)

def test_E():
	assert dos_menores ([2,1,5,9]) == (1,2)
	
def test_F():
	assert dos_menores ([4,6,5]) == (4,5)
	
def test_G():
	assert dos_menores([1,2,3]) == (1,2)
