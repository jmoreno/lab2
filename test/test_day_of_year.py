from day_of_year import day_of_year
import pytest

@pytest.mark.parametrize(["day", "month", "year"], [(None, None, None), (None, 11, 2012), (2, None, 2012), (2, 11, None), (2, 13, 1998), (-1, 12, 2013), (1, 1, 0)])
def test_A(day, month, year):
	with pytest.raises(Exception):
		assert day_of_year(day,month,year)

def test_TypeError():
	with pytest.raises(TypeError):
		assert day_of_year("1",1,2015)

def test_B():
	assert day_of_year(15,1,2013) == 15
	
def test_C():
	assert day_of_year(3,3,2014) == 62

def test_D():
	assert day_of_year(3,3,2024) == 63
