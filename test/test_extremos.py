from extremos import extremos

def test_A1():
    assert extremos() == None

def test_A2():
    assert extremos([]) == None

def test_B():
    assert extremos([1]) == (1,1)

def test_C():
    assert extremos([1,2]) == (1,2)

def test_E():
   assert extremos([3,1,2]) == (1,3)

def test_D():
   assert extremos([2,1]) == (1,2)


