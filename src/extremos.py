def extremos(a = None):
    if type(a) != list or len(a) < 1:  # 1
        return None

    min = a[0]
    max = a[0]

    for i in range(1, len(a)):         # 2
        if a[i] > max:                 # 3
            max = a[i]
        elif min < a[i]:               # 4
            min = a[i]
    return min, max


