class InvalidArgument(Exception):
	"Function called with invalid arguments"
	pass
	
def max_dist(a = None):
	if type(a) != list or len(a) < 2:
        	raise InvalidArgument       	
        
	if len(a) == 2:
        	return abs(a[0] - a[1])
        
	dist = 0
	e1 = a[0]
	for i in range(1, len(a)):
		e2 = a[i]
		if abs(e2 - e1) > dist:
			dist = abs(e2 - e1)
		e1 = a[i]
	return dist
		
