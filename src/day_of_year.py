class InvalidArgument(Exception):
	"Function called with invalid arguments"
	pass

def day_of_year(day,month,year):

	if day == None or month == None or year == None: 
		raise InvalidArgument
	
	if day < 1 or day > 31 or month < 1 or month > 12 or year < 1:
		raise InvalidArgument
	
	days_month = [31,28,31,30,31,30,31,31,30,31,30,31] # Lista con los días de cada mes
	
	position = 0
	
	if month == 1:	# Sentencia para el mes de enero
		return position + day
		
	for i in range(0, month-1):
		position = position + days_month[i] # Acumulado de los días de cada mes
	
	position = position + day # Suma del día del mes actual
	
	if year % 4 == 0 and (year % 100 != 0 or year % 400 == 0): # Comprobar si es año bisiesto
		if month > 2:
			position = position + 1
		if month == 2 and day > 28:
			position = position +1
	return position
